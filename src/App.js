import React from 'react';
import './App.css';
import IndeedContainer from './components/IndeedContainer';

function App() {
  return <IndeedContainer></IndeedContainer>
}

export default App;
