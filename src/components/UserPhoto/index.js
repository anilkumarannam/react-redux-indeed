import { useSelector } from "react-redux";
import { selectFormData } from "../InputField/inputFieldSlice";
import './userPhoto.css';
const UserPhoto = (props) => {

  const { imageUrl, setImage } = props;
  const inputValue = useSelector(selectFormData);
  const userName = (inputValue["first-name"] || "") + " " + (inputValue["last-name"] || "");
  const fileUpload = event => {
    if (event.target.files && event.target.files[0]) {
      const img = event.target.files[0];
      const uploadImageUrl = URL.createObjectURL(img);
      setImage(uploadImageUrl);
    }
  };

  return <>
    <div className='form-container'>
      <h1>User Photo</h1>
      <p>Upload your profile picture and show yourself.</p>
      <form className='input-form' action='#'>
        <div className='profile-image-container'>
          <div className='user-profile'>
            <div className='profile-banner'></div>
            <div className='image-and-name'>
              <img src={imageUrl} alt="UserPhoto" />
              <h1>{userName}</h1>
              <p>Hyderabad</p>
              <input type="file" onChange={fileUpload}></input>
            </div>
          </div>
        </div>
      </form>
    </div>
  </>
}

export default UserPhoto;