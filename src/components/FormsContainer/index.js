import UserPhoto from "../UserPhoto";
import WorkExperience from "../WorkExperience";
import EducationDetails from "../EducationDetails";
import PersonalDetails from "../PersonalDetails";
import { useState } from "react";

const FormsContainer = (props) => {

  const { formName } = props;

  const [profileUrl, setProfileUrl] = useState("#");

  const setImageUrl = (imageUrl) => {
    setProfileUrl(imageUrl);
  }

  const [educationData, setEducationData] = useState([{
    id: "edu-graduation",
    label: "Graduation/Degree",
    deleteIcon: false
  }]);

  const addEducationDetails = () => {
    const id = "edu-school- " + educationData.length;
    const label = "School - " + educationData.length;
    educationData.push({ id, label, deleteIcon: true });
    setEducationData([...educationData]);
  }

  if (formName === "personal-details") {
    return <PersonalDetails />

  } else if (formName === "education-details") {
    return <EducationDetails educationData={educationData} addEducationDetails={addEducationDetails} />

  } else if (formName === "work-experience") {
    return <WorkExperience />

  } else if (formName === "user-photo") {
    return <UserPhoto imageUrl={profileUrl} setImage={setImageUrl} />
  }
}

export default FormsContainer;