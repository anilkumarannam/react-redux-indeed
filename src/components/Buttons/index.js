import './buttons.css';
const Buttons = (props) => {
  const { next, previous } = props;
  return (
    <>
      <div className="indeed-buttons">
        <button className='white' onClick={previous}>Back</button>
        <button className='blue' onClick={next}>Next Step</button>
      </div>
    </>
  );
}
export default Buttons;