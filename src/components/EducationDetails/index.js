import InputField from "../InputField";
import './educationDetails.css';

const EducationDetails = (props) => {

  const schools = props.educationData;

  return <>
    <div className='form-container'>
      <h1>Education</h1>
      <p>Inform companies about your education life.</p>
      <form className='input-form edu' action='#'>
        {schools.map((school) => <InputField
          key={school.id}
          id={school.id}
          label={school.label}
          type="text"
          deleteIcon={school.deleteIcon}
        />)}
      </form>
      <button onClick={props.addEducationDetails}>Add New School</button>
    </div>
  </>
}

export default EducationDetails;