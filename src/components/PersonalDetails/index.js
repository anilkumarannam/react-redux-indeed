import { useState } from "react";
import InputField from "../InputField";
import SelectInput from '../SelectInput';
import optionsData from "./optionsData";

import { useSelector } from "react-redux";
import { selectFormData } from "../InputField/inputFieldSlice";

const PersonalDetails = () => {
  const { countries } = optionsData;

  const inputValue = useSelector(selectFormData);
  const countryCode = inputValue["select-country"];
  let countryCities = [];
  if (countryCode !== "none" && (countryCode)) {
    countryCities = optionsData.cities[countryCode];
    countryCities = countryCities.map((city) => ({ code: city, name: city }));
  }
  const [cities, updateList] = useState(countryCities);
  const updateCities = (code) => {
    if (code !== "none") {
      const countryCities = optionsData.cities[code];
      const updatedCities = countryCities.map((city) => ({ code: city, name: city }));
      updateList(updatedCities);
    } else {
      updateList([]);
    }
  }

  return <>
    <div className='form-container'>
      <h1>Your Personal Information</h1>
      <p>Enter your personal details to get closer to companies.</p>
      <form className='input-form' action='#'>
        <InputField id="first-name" label="First Name" type="text" />
        <InputField id="last-name" label="Last Name" type="text" />
        <InputField id="phone-number" label="Phone Number" type="tel" />
        <InputField id="email-address" label="Email Address" type="email" />
        <SelectInput id="select-country" label="Country" title="Select Country" optionData={countries} changeAction={updateCities} />
        <SelectInput id="select-city" label="City" title="Select City" optionData={cities} />
      </form>
    </div>
  </>
}
export default PersonalDetails;