const optionsData = {
  countries: [
    { code: "IND", name: "India" },
    { code: "ISR", name: "Israel" },
    { code: "RUS", name: "Russia" }
  ],
  cities: {
    "IND": ["Hyderabad", "Banglore", "Delhi", "Pune", "Chennai"],
    "ISR": ["Jerusalem", "Haifa"],
    "RUS": ["Moscow", "Saint Petersburg", "Novosibirsk"]
  }
}
module.exports = optionsData;