import InputField from "../InputField"
const WorkExperience = () => {
  return <>
    <div className='form-container'>
      <h1>Work Experience</h1>
      <p>Inform companies about your education life.</p>
      <form className='input-form' action='#'>
        <InputField id="experience-one" label="Experience - 1" type="text" />
        <InputField id="position-one" label="Position" type="text" />
        <InputField id="experience-two" label="Experience - 2" type="text" />
        <InputField id="position-two" label="Position" type="text" />
      </form>
    </div>
  </>
}

export default WorkExperience;