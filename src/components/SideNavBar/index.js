const SideNavBar = (props) => {
  const { activeIndex } = props;
  const activeControl = ["step", "step", "step", "step"];
  activeControl[activeIndex] = "step active";
  return <>
    <div className='side-nav'>
      <img src="indeed-logo.jpg" className='indeed-logo' alt="Indeed Logo - White Indeed Logo@pngkit.com"></img>
      <div className="steps-container">
        <div className='steps'>
          <h1>Step {activeIndex + 1}</h1>
          <p>Enter your personal details to get closer to companies.</p>
          <div className={activeControl[0]}>
            <div className='number'>
              <span>1</span>
            </div>
            <p> Personal Information</p>
          </div>
          <div className='line'></div>
          <div className={activeControl[1]}>
            <div className='number'>
              <span>2</span>
            </div>
            <p> Education</p>
          </div>
          <div className='line'></div>
          <div className={activeControl[2]}>
            <div className='number'>
              <span>3</span>
            </div>
            <p> Work Experience</p>
          </div>
          <div className='line'></div>
          <div className={activeControl[3]}>
            <div className='number'>
              <span>4</span>
            </div>
            <p> User Photo</p>
          </div>
        </div>
      </div>
    </div>
  </>;
}

export default SideNavBar;