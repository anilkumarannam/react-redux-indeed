import { useState } from 'react';
import FormsContainer from '../FormsContainer';
import Buttons from '../Buttons';
import './indeedContainer.css';
import SideNavBar from '../SideNavBar';
const IndeedContainer = () => {

  const [state, changeFormName] = useState({ formName: "personal-details", activeIndex: 0 });

  const nextForm = (event) => {
    switch (state.formName) {
      case "personal-details":
        changeFormName({ formName: "education-details", activeIndex: 1 });
        break;
      case "education-details":
        changeFormName({ formName: "work-experience", activeIndex: 2 });
        break;
      case "work-experience":
        changeFormName({ formName: "user-photo", activeIndex: 3 });
        break;
      default: ;
    }
  };

  const previousForm = (event) => {
    switch (state.formName) {
      case "education-details":
        changeFormName({ formName: "personal-details", activeIndex: 0 });
        break;
      case "work-experience":
        changeFormName({ formName: "education-details", activeIndex: 1 });
        break;
      case "user-photo":
        changeFormName({ formName: "work-experience", activeIndex: 2 });
        break;
      default: ;
    }
  };

  return (

    <div className='row content'>
      <SideNavBar activeIndex={state.activeIndex} />
      <div className='forms-row'>
        <div>
          <FormsContainer formName={state.formName} />
          <Buttons next={nextForm} previous={previousForm} />
        </div>
      </div>
    </div>
  );


}

export default IndeedContainer;
