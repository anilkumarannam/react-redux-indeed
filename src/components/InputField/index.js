import { useDispatch, useSelector } from "react-redux";
import { update, selectFormData } from "./inputFieldSlice";
import './inputField.css';
const InputField = (props) => {
  const { id, label, type, deleteIcon } = props;
  const dispatch = useDispatch();
  const inputValue = useSelector(selectFormData);
  const inputFieldClass = deleteIcon ? "delete-icon" : "";
  return (<>
    <div className="input-field">
      <label htmlFor={id}>{label}</label>
      <div className={inputFieldClass}>
        <input value={inputValue[id] || ""} id={id} type={type} onChange={(event) => dispatch(update({ id, value: event.target.value }))}></input>
        {deleteIcon && <img src="https://img.icons8.com/color/48/000000/minus.png" alt="DeleteIcon" />}
      </div>
    </div>
  </>);
}
export default InputField;
