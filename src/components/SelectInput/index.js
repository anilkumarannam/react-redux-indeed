import { useDispatch, useSelector } from "react-redux";
import { update, selectFormData } from "../InputField/inputFieldSlice";
import './selectInput.css';

const SelectInput = (props) => {
  const { id, label, title, optionData, changeAction } = props;
  const dispatch = useDispatch();
  const inputValue = useSelector(selectFormData);
  const changeCountry = (event) => {
    dispatch(update({ id, value: event.target.value }));
    if (changeAction) {
      dispatch(update({ id: "select-city", value: "none" }));
      changeAction(event.target.value);
    }
  }
  return (<>
    <div className="select-input">
      <label htmlFor={id}>{label}</label>
      <select id={id} onChange={changeCountry} value={inputValue[id] || "none"}>
        <option value="none">{title}</option>
        {optionData.map((item) => <option value={item.code} key={item.code}>{item.name}</option>)}
      </select>
    </div>
  </>);
}
export default SelectInput;