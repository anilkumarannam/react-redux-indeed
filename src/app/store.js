import { configureStore } from '@reduxjs/toolkit';
import inputFieldSlice from '../components/InputField/inputFieldSlice';
export const store = configureStore({
  reducer: { inputFieldSlice },
});
